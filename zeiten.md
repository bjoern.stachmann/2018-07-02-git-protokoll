# Zeiten

## Montag

  * 09:30 GIT
          (1 h)
  * 10:30 PAUSE
          (15 min)

  * 10:45 GIT
          (1,25 h)
  * 12:00 MITTAG
          (45 min)

  * 12:45 GIT
          (1,25 h)
  * 14:00 PAUSE
          (15 min)

  * 14:15 GIT
          (1,25 h)
  * 15:30 PAUSE
          (15 min)

  * 15:45 GIT
          (1,25 h)
  * 17:00 FEIERABEND

(Optional)

  * 17:00 PAUSE
          (15 min)
  * 17:15 GIT: Git, wie funktioniert das eigentlich?
          (45 min)
  * 18:00 FEIERABEND

  ## Dienstag

    * 09:00 GIT
            (1,5 h)
    * 10:30 PAUSE
            (15 min)

    * 10:45 GIT
            (1,25 h)
    * 12:00 MITTAG
            (45 min)

    * 12:45 GIT
            (1,5h)
    * 14:15 PAUSE
            (15 min)

    * 14:30 GIT
            (1,5 h)
    * 16:00 FEIERABEND
