# Lokales Repo auf dem Trainer-Rechner

## Setup

    root # adduser gituser

    gituser $ git init --bare protokoll.git



## Benutzung

  * User: gituser
    * Password: torvalds

    $ git clone ssh://gituser@127.0.0.1/~/protokoll.git

Mehr Komfort: Public-Key in `authorized_keys` eintragen.

  ssh-copy-id gituser@127.0.0.1
