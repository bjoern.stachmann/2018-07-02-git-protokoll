# Git-Workshop

## Intro Git

 * Torvalds und die Entstehung von Git
 * Wichtige Eigenschaften von Git
   - dezentral
   - performant
   - robust und zuverlässig
   - open source
   - flexibel
   - "easy to contribute"
   - verteiltes Backup

## Quickstart

 1. Repo anlegen
        git init myrepo
 1. Datei anlegen
 1. Staging (Commit vorbereiten)
        git add myfile
 1. Commit
        git commit -m "A new file"
 1. Datei ändern
 1. Staging (Commit vorbereiten)
        git add myfile
 1. Nochmal Commit
        git commit -m "A new file"
 1. Änderungen ansehen
        git log
 1. Checkout älterer Versionen
        git checkout 9af249af24edd
 1. Zurück auf den `master`
        git checkout master

### Tipps

 * `log --oneline`: Einzeilige Darstellung
 * `git reflog`: Zeigt alle Änderungen

## Commit

 * `git commit -am "a message"`
   = add + commit

## Staging: Commits zusammenstellen.

 * *Workspace*
   Dateien und Verzeichnisse des Projekts
   Befehle:
   - `git checkout 7a990e2 -- myfile`
   - `git diff`: Vergleich Workspace mit `HEAD`
 * *Stage* Area/Index
   - `git add myfile`
   - `git reset HEAD -- myfile`
   - `git diff --staged`
 * *Repository*
   - `git commit -m "message"`
 * *aktiver Branch
   - `reset und `commit` verändern den aktiven Branch
   - `checkout` wechselt den aktiven Branch

### Tipps

 * Simplified *Staging*
    - `git add myfile`: fÜr neu Dateien
    - `git commit -am "a message"`
 * `.gitignore`: Generierte und temporäre Dateien auf


## Mehr über das Repository

Begriffe

 * Commit, Tree (Verzeichnis), Blob (Dateiinhalt)
 * Object Storage
   - Key: SHA1-Hash des Inhalts
   - Value: Content (gzip)

![Object Storage](object-storage.png)

## Branching & Merging

Branches

 * `git branch mybranch`: Neuer Branch
 * `git branch`: Liste der Branches
 * `git checkout mybranch`: Aktiven Branch wechseln
 * `git `

Merges

 * `git merge otherbranch`
 * Fast-Forward. Wenn nur auf `otherbranch` Neuerungen sind, wird der aktive Branch hochgesetzt,
   ohne
 * Nach manueller Konfliktauflösung: `git add resolved file`, dann `git commit`

Vermeiden von kniffligen Konflikten:

 * kurzlebige Branches
 * spezialisierte Branches (klare Aufgabe)
 * feingranulare Commits
   - Trenne Refactorings von Features und Bugfixes.

![Merge Conflicts](merge-conflicts.png)

### Tipps

  * `--theirs, --ours, bei `
 Merge Strategien (theirs, ours)

## Push/Pull

 * `git push origin master`: Überträgt den lokalen `master`-Stand zum Server
 * `git fetch origin`: Holt Informationen über ein anderes Repo.
   - *Remote Branches* zeigen an, wie Branches im anderen Repo stehen.
   - Es wird angezeigt, welche Neuerungen geholt wurden, z. B. `3aff327..87ee1a1`.
     Taugt als Parameter für `git log 3aff327..87ee1a1`.
 * *Verknüpfte Branches*
   - dorthin wird push/pull/fetch ausgeführt, wenn kein Zielrepo angegeben wird.
   - `git status`: Zeigt `ahead` und `behind` bezogen auf den verknüpften Branch.
 * `git push`: Commit auf Zielbanch muss Nachfolger des vorigen Commit sein.
 * `git pull` = Fetch + Merge

![Push-Pull](push-pull.png)

## Rebasing

![Rebse](rebase.png)

 * `git rebase origin/master`:
   Kopiert alle Commits vom aktuellen Branch, die noch nicht in `origin/master`
   enthalten sind erstelllt sie beginnen ab `origin/master` neu.
   Das sieht dann nachher so aus, als ob der Branch erst dort abgezweigt wäre.

 * `merge` und `rebase` sind eng verwandt:
   Wenn man einen Feature-Branch nicht am Stück mergen würde, sondern Commit,
   dann erhält man fast dieselbe Folge von neuen Commits.
   Es werden identische Tree-Objekte erzeigt.
   Aber Die Merge-Commits haben das ursprüngliche Commit als zweiten Parent,
   die Rebase-Commits erscheinen wie normale Commits nur einem Parent.

 * `git rebase --interactive origin/master`
   Die Commits werden nicht nur kopiert,
   sondern man kann sie vorher manipulieren:
   Message Ändern, Löschen, Reihenfolge Ändern, Zusammenfassen.
   Nützlich, um eine  Commit-Histoie für ein Review lesbar zu machen.

![Rebasing Probleme](rebase-probleme.png)

## GUIs

Im Seminare verwenedet wurden:

 * Atom Editor
 * IntelliJ Idea

## Tipps und Tricks

 * Tags
 * Stashing
 * Versionen spefizieren
 * Refspecs

## Branching Strategien

### Feature-Branching

 * Pull-Request/Code Reviews

### Gemeinsam auf master

### Gitflow

### Forking

### Zusammenfassung, Tipps zum Branching

## Strategien für große Projekte

### Spezialfall große Dateien: LFS

### Submodules

### Subrepos

## Git Administration & Advanced Git

 * `git revert 34ffa789a`: rückgängig machen von Commits.
   ![Revert](revert-2.png)
 * `git filter-branch`
   - Alternative (für einige Fälle) [BFG](https://rtyley.github.io/bfg-repo-cleaner/)


TODO Windows CR+LF

## Anforderungen

### Einführung in die Versionsverwaltung mit Git

- [x] Grundbegriffe wie Repository, Commit, Tag, Branch und Merge
- [x] Bedeutung des Releasemanagements
- [x] Vergleich mit einem zentralen Ansatz (z.B. svn)

Versionierung von Dateien mit Git
- [x] Erstellung eines Repositories (Init)
- [x] Grundlegender Versionsverwaltungs-Workflow (Add, Commit)
- [x] Änderungen verfolgen (Log)
- [x] Umgang mit Enwicklungszweigen (Branches, Merge) und Versionen (Tags)

### Sonstiges

- [x] Die Kommunikation zwischen Repositorys (Clone, Fetch, Pull,Push)
- [x] Umsetzung eines zentralen Repositorys (Bare Repository)
- [x] Erweiterte Git Funktionen (Rebase, Cherry-Pick, Stash, Reflog, Diff)
- [x] Ändern/Änderungen rückgängig machen (Amend Commit)
- [x] Ändern/Änderungen rückgängig machen (Revert)
- [x] Zurücksetzen des Arbeitsbereichs und von Entwicklungszweigen
(Reset)
- [ ] Wiederkehrende Schemata verschiedener Git Kommandos
(Refspecs, Version Ranges, ...)
  - [x] Version Ranges

### Best Practices und Begrifflichkeiten
- [ ] Separation of concerns
- [x] Commit early & commit often
- [x] Topic-Branches
- [x] Merge vs. Rebase
- [x] Up vs Down-Merge
- [x] Fork vs Branch
- [x] Pull Requests und deren Verwaltung (Stichwort Gatekeeper)
- [x] Code Reviews in den Workflow integrieren

### Konzepte und Tooling
- [x] Git und CVS/Subversion
- [ ] Überblick gängiger graphischer Clients (Git GUI, Atlassian)

### Weiterführende Themen ####

#### Erweiterte Konzepte und Optimierungen
- [x] Verschachteln von Repositorys (Submodules)
- [x] Strukturierung von Repositories und deren Optimierung
- [x] Shallow Clones
- [x] Sparse Checkouts
- [ ] Git Repository Internals
- [x] Merge Strategien
  (theirs, ours)

#### evtl. Gitflow als Branching- und Workflow-Konzept

- [x] Aufgaben von Entwicklungszweigen (Branching Modell)
- [x] Vorgehen und Workflow bei der Umsetzung von Änderungen
- [ ] Tool support
